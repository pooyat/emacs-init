(setq straight-repository-branch "develop")
(setq straight-use-package-by-default t)

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
	(url-retrieve-synchronously
	 "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
	 'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; install use-package
(straight-use-package 'use-package)

(use-package evil
  :config
  (evil-mode 1))

(straight-use-package 'ess)

(use-package tex-site
  :straight auctex
  :custom
  (TeX-view-program-selection '((output-pdf "Zathura"))))

;; superior lisp interaction mode for emacs
;; to evaluate lisp code START SLIME: M-x slime
(use-package slime
  :init
  (setq inferior-lisp-program "sbcl --noinform"))

(straight-use-package 'eglot)

;; elpy: emacs python development environment
(use-package elpy
  :defer t
  :init
  (advice-add 'python-mode :before 'elpy-enable))

;; save minibuffer history between sessions
(savehist-mode 1)
(setq savehist-additional-variables '(kill-ring search-ring regexp-search-ring))

(menu-bar-mode -1)  ; disable the menubar

;; only to peruse an updated list-packages
;; otherwise do NOT use package.el.  Use straight.el instead
(require 'package)
(setq package-enable-at-startup nil)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/"))

(electric-pair-mode 1)	 ; automatically type right ), ], }, ", and '.
(column-number-mode 1)	 ; put column number in mode bar

(add-hook 'prog-mode-hook 'display-line-numbers-mode) ; display line numbers when coding
(setq fill-column 50)	      ; wrap lines at 79th character using M-q
(add-hook 'prog-mode-hook 'auto-fill-mode) ; insert a line ending after the last word that occurs before the value of option fill-column

(add-hook 'text-mode-hook (		; visually wrap long lines in text files
			   lambda()
				 (visual-line-mode 1)))

(setq-default indent-tabs-mode nil)

(setq c-default-style "linux"
      c-basic-offset 4)

(tool-bar-mode -1)			; Do not show toolbar

;; active Babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((R . t)
   (python . t)
   (lisp . t)         ; to evaluate lisp code START SLIME: M-x slime
   (C . t)
   (java . t)
   ))

(add-hook 'org-babel-after-execute-hook 'org-display-inline-images 'append)

(setq org-confirm-babel-evaluate nil)
(setq org-export-babel-evaluate nil)

;; Allow multiple line Org emphasis markup
(setcar (nthcdr 4 org-emphasis-regexp-components) 10)
(org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)

(setq org-list-allow-alphabetical t)
